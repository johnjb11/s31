//contain all business logic


const Task = require('../models/taskSchema')


//get all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}


//creating a new task

module.exports.createTask = (reqBody) => {

	let newTask = new Task ({

		name: reqBody.name
	})

		return newTask.save().then((task,err) => {

			if(err){
				console.log(err)
			}else{
				return task
			}
		})
}


//delete task

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removedTask, err)=>{

		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}


//update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask,saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedTask
				}
			})
	})
}



//s31 activity

module.exports.findTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	})
}


module.exports.changeStatusTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.status = newContent.status
			return result.save().then((changedTask,saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return changedTask
				}
			})
	})
}