//contains all task endpoints for our applications

const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')

router.get('/', (req,res) => {

	taskController.getAllTasks()
	.then(resultFromController => res.send(resultFromController))
})

router.post('/createTask', (req,res) => {

	taskController.createTask(req.body)
	.then(resultFromController => res.send(resultFromController))
})


router.delete('/deleteTask/:id', (req,res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


//route for updating task

router.put('/updateTask/:id', (req,res) =>{

	taskController.updateTask(req.params.id, req.body)
	.then(resultFromController => res.send(resultFromController))
})








//activity

router.get('/:id', (req,res) =>{

	taskController.findTask(req.params.id, req.body)
	.then(resultFromController => res.send(resultFromController))
})



router.put('/:id/complete', (req,res) =>{

	taskController.changeStatusTask(req.params.id, req.body)
	.then(resultFromController => res.send(resultFromController))
})







module.exports = router

